package Inheritance;

public final class CEO extends Manager{
    private int test;
    public CEO(String name)
    {
        super(name);
    }

    public int getTest()
    {
        return this.test;
    }
}
//a n b = a
//a n b != tohi
//inheritance -> is-a