package Exception;

import java.util.Scanner;

public class Excption {
    //
    public static void main(String[] args) {
        //syntax
//        String s;
        //logical
//        System.out.println(pow(10));
//        System.out.println(pow(12));
//        System.out.println(pow(25000000));
        //exception
        //I/O -- Runtime
        //checked -- unchecked
//        try {
//            System.out.println(div(12,0));
//            int[] nums = new int[]{1, 2, 3};
//            System.out.println(nums[3]);
//            Test test;
//            System.out.println(test.a);

//        }
//        catch (Exception e)
//        {
//            System.out.println(e.getMessage());
//        }
//        finally {
//            System.out.println("try catch finished");
        System.out.println(div(12,0));
        }

    public static int pow(int a)
    {
        return a*a;
    }
    public static int div(int a, int b)
    {
        try {
            return a/b;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {
            return 1;
        }
    }
}
